!pip install git+https://github.com/raghakot/keras-vis.git -U
import os

import numpy as np
import pandas as pd
import os
import cv2
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import random
from sklearn.utils import shuffle
from tqdm import tqdm_notebook
from PIL import Image

import glob

import tensorflow as tf

from keras import backend as KBackend
from keras.preprocessing import image
from keras.preprocessing.image import ImageDataGenerator
from keras.callbacks import ModelCheckpoint
from keras.backend.tensorflow_backend import set_session
from keras.optimizers import SGD, Adam
from keras.layers import Dense,GlobalAveragePooling2D, Conv2D, MaxPooling2D, Dropout, Flatten, Input
from keras.models import Model, Sequential

from sklearn.model_selection import train_test_split

config = tf.ConfigProto()
config.gpu_options.allow_growth = True
sess = tf.Session(config=config)
set_session(sess)

!mkdir -p ~/.kaggle
!cp kaggle.json ~/.kaggle/
!chmod 600 ~/.kaggle/kaggle.json
!ls ~/.kaggle

!ls -l ~/.kaggle
!cat ~/.kaggle/kaggle.json

!kaggle competitions download -c histopathologic-cancer-detection
%%capture
!unzip /content/train.zip -d train

%%capture
!unzip /content/test.zip -d test

!unzip /content/train_labels.csv.zip 

!unzip /content/sample_submission.csv.zip 

%cd train
!mkdir tumour
!mkdir no_tumour
!ls -d */
%cd ..

!mkdir validation
%cd validation
!mkdir tumour
!mkdir no_tumour
%cd ..

labels = pd.read_csv('train_labels.csv')

train_path = './train/'
test_path = './test'
validation_path = './validation'

tumour_train_path = './train/tumour/'
no_tumour_train_path = './train/no_tumour/'

tumour_val_path = './validation/tumour/'
no_tumour_val_path = './validation/no_tumour/'

SAMPLE_SUB = 'sample_submission.csv'

# quick look at the label stats
labels['label'].value_counts()

tumour_df = labels[labels.label == 1]
no_tumour_df = labels[labels.label == 0]

#@title
# As we count the statistics, we can check if there are any completely black or white images
dark_th = 10 / 255      # If no pixel reaches this threshold, image is considered too dark
bright_th = 245 / 255   # If no pixel is under this threshold, image is considerd too bright
too_dark_idx = []
too_bright_idx = []

x_tot = np.zeros(3)
x2_tot = np.zeros(3)
counted_ones = 0
for i, idx in tqdm_notebook(enumerate(shuffled_data['id']), 'computing statistics...(220025 it total)'):
    path = os.path.join(tumour_train_path, idx)
    imagearray = readCroppedImage(path + '.tif', augmentations = False).reshape(-1,3)
    # is this too dark
    if(imagearray.max() < dark_th):
        too_dark_idx.append(idx)
        continue # do not include in statistics
    # is this too bright
    if(imagearray.min() > bright_th):
        too_bright_idx.append(idx)
        continue # do not include in statistics
    x_tot += imagearray.mean(axis=0)
    x2_tot += (imagearray**2).mean(axis=0)
    counted_ones += 1
    
channel_avr = x_tot/counted_ones
channel_std = np.sqrt(x2_tot/counted_ones - channel_avr**2)
channel_avr,channel_std

#If removing outliers, uncomment the four lines below
print('Before removing outliers we had {0} training samples.'.format(train_df.shape[0]))
train_df = train_df.drop(labels=too_dark_idx, axis=0)
train_df = train_df.drop(labels=too_bright_idx, axis=0)
print('After removing outliers we have {0} training samples.'.format(train_df.shape[0]))

for filename, class_name in labels.values:
  
  filename = filename + '.tif'
  
  if class_name == 1:
        os.rename(train_path + filename, tumour_train_path + filename)
  else:
        os.rename(train_path + filename, no_tumour_train_path + filename)
		
def readImage(path):
    # OpenCV reads the image in bgr format by default
    bgr_img = cv2.imread(path, cv2.IMREAD_COLOR)
    print(path)
    # We flip it to rgb for visualization purposes
    b,g,r = cv2.split(bgr_img)
    rgb_img = cv2.merge([r,g,b]) 
    return rgb_img

%matplotlib inline
#Disable grid 
plt.rcParams["axes.grid"] = False   

# random sampling
shuffled_data = shuffle(labels)

fig, ax = plt.subplots(2,5, figsize=(20,8))
fig.patch.set_facecolor('xkcd:white')
fig.suptitle('Histopathologic scans of lymph node sections',fontsize=20)
# Negatives
for i, idx in enumerate(shuffled_data[shuffled_data['label'] == 0]['id'][:5]):
    path = os.path.join(no_tumour_train_path, idx)
    ax[0,i].imshow(readImage(path + '.tif'))
    # Create a Rectangle patch
    box = patches.Rectangle((32,32),32,32,linewidth=4,edgecolor='b',facecolor='none', linestyle=':', capstyle='round')
    ax[0,i].add_patch(box)
ax[0,0].set_ylabel('Negative samples', size='large')
# Positives
for i, idx in enumerate(shuffled_data[shuffled_data['label'] == 1]['id'][:5]):
    path = os.path.join(tumour_train_path, idx)
    ax[1,i].imshow(readImage(path + '.tif'))
    # Create a Rectangle patch
    box = patches.Rectangle((32,32),32,32,linewidth=4,edgecolor='r',facecolor='none', linestyle=':', capstyle='round')
    ax[1,i].add_patch(box)
ax[1,0].set_ylabel('Tumor tissue samples', size='large')

#@title
import random
ORIGINAL_SIZE = 96      # original size of the images - do not change

# AUGMENTATION VARIABLES
CROP_SIZE = 90          # final size after crop
RANDOM_ROTATION = 3    # range (0-180), 180 allows all rotation variations, 0=no change
RANDOM_SHIFT = 2        # center crop shift in x and y axes, 0=no change. This cannot be more than (ORIGINAL_SIZE - CROP_SIZE)//2 
RANDOM_BRIGHTNESS = 7  # range (0-100), 0=no change
RANDOM_CONTRAST = 5    # range (0-100), 0=no change
RANDOM_90_DEG_TURN = 1  # 0 or 1= random turn to left or right

def readCroppedImage(path, augmentations = True):
    # augmentations parameter is included for counting statistics from images, where we don't want augmentations
    
    # OpenCV reads the image in bgr format by default
    bgr_img = cv2.imread(path)
    # We flip it to rgb for visualization purposes
    b,g,r = cv2.split(bgr_img)
    rgb_img = cv2.merge([r,g,b])
    
    if(not augmentations):
        return rgb_img / 255
    
    #random rotation
    rotation = random.randint(-RANDOM_ROTATION,RANDOM_ROTATION)
    if(RANDOM_90_DEG_TURN == 1):
        rotation += random.randint(-1,1) * 90
    M = cv2.getRotationMatrix2D((48,48),rotation,1)   # the center point is the rotation anchor
    rgb_img = cv2.warpAffine(rgb_img,M,(96,96))
    
    #random x,y-shift
    x = random.randint(-RANDOM_SHIFT, RANDOM_SHIFT)
    y = random.randint(-RANDOM_SHIFT, RANDOM_SHIFT)
    
    # crop to center and normalize to 0-1 range
    start_crop = (ORIGINAL_SIZE - CROP_SIZE) // 2
    end_crop = start_crop + CROP_SIZE
    rgb_img = rgb_img[(start_crop + x):(end_crop + x), (start_crop + y):(end_crop + y)] / 255
    
    # Random flip
    flip_hor = bool(random.getrandbits(1))
    flip_ver = bool(random.getrandbits(1))
    if(flip_hor):
        rgb_img = rgb_img[:, ::-1]
    if(flip_ver):
        rgb_img = rgb_img[::-1, :]
        
    # Random brightness
    br = random.randint(-RANDOM_BRIGHTNESS, RANDOM_BRIGHTNESS) / 100.
    rgb_img = rgb_img + br
    
    # Random contrast
    cr = 1.0 + random.randint(-RANDOM_CONTRAST, RANDOM_CONTRAST) / 100.
    rgb_img = rgb_img * cr
    
    # clip values to 0-1 range
    rgb_img = np.clip(rgb_img, 0, 1.0)
    
    return rgb_img
	
#@title
fig, ax = plt.subplots(2,5, figsize=(20,8))
fig.patch.set_facecolor('xkcd:white')
fig.suptitle('Cropped histopathologic scans of lymph node sections',fontsize=20)
# Negatives
for i, idx in enumerate(shuffled_data[shuffled_data['label'] == 0]['id'][:5]):
    path = os.path.join(no_tumour_train_path, idx)
    ax[0,i].imshow(readCroppedImage(path + '.tif'))
ax[0,0].set_ylabel('Negative samples', size='large')
# Positives
for i, idx in enumerate(shuffled_data[shuffled_data['label'] == 1]['id'][:5]):
    path = os.path.join(tumour_train_path, idx)
    ax[1,i].imshow(readCroppedImage(path + '.tif'))
ax[1,0].set_ylabel('Tumor tissue samples', size='large')

#fig, ax = plt.subplots(1,5, figsize=(20,4))
#fig.patch.set_facecolor('xkcd:white')
#fig.suptitle('Random augmentations to the same image',fontsize=20)
# Negatives
#for i, idx in enumerate(shuffled_data[shuffled_data['label'] == 0]['id'][:1]):
#    for j in range(5):
#        path = os.path.join(no_tumour_train_path, idx)
#        ax[j].imshow(readCroppedImage(path + '.tif'))

from sklearn.model_selection import train_test_split

# we read the csv file earlier to pandas dataframe, now we set index to id so we can perform
train_df = data.set_index('id')

#If removing outliers, uncomment the four lines below
#print('Before removing outliers we had {0} training samples.'.format(train_df.shape[0]))
#train_df = train_df.drop(labels=too_dark_idx, axis=0)
#train_df = train_df.drop(labels=too_bright_idx, axis=0)
#print('After removing outliers we have {0} training samples.'.format(train_df.shape[0]))

train_names = train_df.index.values
train_labels = np.asarray(train_df['label'].values)

# split, this function returns more than we need as we only need the validation indexes for fastai
tr_n, tr_idx, val_n, val_idx = train_test_split(train_names, range(len(train_names)), test_size=0.1, stratify=train_labels, random_state=123)



for filename in tumour_df.values[1 : round(len(tumour_df) / 5)]:
  filename = filename[0] + '.tif'
  os.rename(tumour_train_path + filename, tumour_val_path + filename)

for filename in no_tumour_df.values[1 : round(len(no_tumour_df) / 5)]:
  filename = filename[0] + '.tif'
  os.rename(no_tumour_train_path + filename, no_tumour_val_path + filename)
  
tumour_image_sample = [cv2.imread(file) for file in glob.glob(tumour_train_path + '*.tif')][:5]
print(tumour_image_sample)

KBackend.image_data_format()

 #Crop to 32x32
BATCH_SIZE = 2000
TARGET_SIZE = (32,32)
EPOCHS = 5
NUM_CLASSES = 2

if KBackend.image_data_format() == 'channels_first':
    INPUT_SHAPE = (3, 32, 32)
else:
    INPUT_SHAPE = (32, 32, 3)

	
datagen = ImageDataGenerator()
    
train_generator = datagen.flow_from_directory(train_path, 
	target_size=TARGET_SIZE, color_mode='rgb', batch_size=BATCH_SIZE, class_mode='categorical', shuffle=True)

val_generator = datagen.flow_from_directory(validation_path, 
	target_size=TARGET_SIZE, color_mode='rgb', batch_size=BATCH_SIZE, class_mode='categorical', shuffle=True)

step_size_train = train_generator.n // train_generator.batch_size
step_size_val = val_generator.n // val_generator.batch_size

model = tf.keras.models.Sequential()

model.add(tf.keras.layers.BatchNormalization(input_shape=INPUT_SHAPE, axis=3))
model.add(tf.keras.layers.Conv2D(32, (3, 3), activation='relu'))
model.add(tf.keras.layers.MaxPooling2D(pool_size=(2, 2)))
model.add(tf.keras.layers.Dropout(0.25))

model.add(tf.keras.layers.BatchNormalization(input_shape=INPUT_SHAPE, axis=3))
model.add(tf.keras.layers.Conv2D(64, (3, 3), activation='relu'))
model.add(tf.keras.layers.MaxPooling2D(pool_size=(2, 2)))
model.add(tf.keras.layers.Dropout(0.25))

#model.add(tf.keras.layers.BatchNormalization(input_shape=INPUT_SHAPE, axis=3))
#model.add(tf.keras.layers.Conv2D(256, (5, 5), activation='relu')
#model.add(tf.keras.layers.MaxPooling2D(pool_size=(2, 2)))
#model.add(tf.keras.layers.Dropout(0.25))

model.add(tf.keras.layers.Flatten())
model.add(tf.keras.layers.Dense(256, activation='relu'))
model.add(tf.keras.layers.Dropout(0.5))

model.add(tf.keras.layers.Dense(NUM_CLASSES, activation='softmax'))


model.summary()


checkpoint = ModelCheckpoint(normal_weight_path, monitor='val_acc', verbose=1, save_best_only=True, mode='max')

model.compile(optimizer='adam', loss='categorical_crossentropy', metrics=['accuracy'])

model.fit_generator(generator=train_generator,
                   steps_per_epoch=step_size_train,
                   epochs=EPOCHS, validation_data=val_generator,
                   validation_steps = step_size_val,
                   shuffle=True)
				   
# This address identifies the TPU we'll use when configuring TensorFlow.
TPU_WORKER = 'grpc://' + os.environ['COLAB_TPU_ADDR']
tf.logging.set_verbosity(tf.logging.INFO)

tpu_model = tf.contrib.tpu.keras_to_tpu_model(
    model,
    strategy=tf.contrib.tpu.TPUDistributionStrategy(
        tf.contrib.cluster_resolver.TPUClusterResolver(tpu='grpc://' + os.environ['COLAB_TPU_ADDR'])
    )
)

tpu_model.compile(
    optimizer=tf.train.AdamOptimizer(learning_rate=1e-3, ),
    loss=tf.keras.losses.categorical_crossentropy,
    metrics=['categorical_accuracy']
)

tpu_model.fit_generator(generator=train_generator,
                   steps_per_epoch=step_size_train,
                   epochs=EPOCHS, validation_data=val_generator,
                   validation_steps = step_size_val,
                   shuffle=True)

				   
# fastai 1.0
from fastai import *
from fastai.vision import *
from torchvision.models import *    # import *=all the models from torchvision  

arch = densenet169                  # specify model architecture, densenet169 seems to perform well for this data but you could experiment
BATCH_SIZE = 128                    # specify batch size, hardware restrics this one. Large batch sizes may run out of GPU memory
sz = CROP_SIZE                      # input size is the crop size
MODEL_PATH = str(arch).split()[1]   # this will extrat the model name as the model file name e.g. 'resnet50'

# create dataframe for the fastai loader
train_dict = {'name': train_path + train_names, 'label': train_labels}
df = pd.DataFrame(data=train_dict)
# create test dataframe
test_names = []
for f in os.listdir(test_path):
    test_names.append(test_path + f)
df_test = pd.DataFrame(np.asarray(test_names), columns=['name'])

# Subclass ImageItemList to use our own image opening function
class MyImageItemList(ImageItemList):
    def open(self, fn:PathOrStr)->Image:
        img = readCroppedImage(fn.replace('//./',''))
        # This ndarray image has to be converted to tensor before passing on as fastai Image, we can use pil2tensor
        return vision.Image(px=pil2tensor(img, np.float32))
    
	
imgDataBunch = ImageDataBunch.from_df(path='/train', df=df, cols='name', suffix='.tif')


verify_images(path = './train', dest = '/content/train_v')



# Create ImageDataBunch using fastai data block API

imgDataBunch = (MyImageItemList
        .from_df(path='/', df=df, cols='name', suffix='.tif')
        #Where to find the data?
        .split_by_idx(val_idx)
        #How to split in train/valid?
        .label_from_df(cols='label')
        #Where are the labels?
        .add_test(MyImageItemList.from_df(path='/', df=df_test))
        #dataframe pointing to the test set?
        .transform(tfms=[[],[]], size=sz)
        # We have our custom transformations implemented in the image loader but we could apply transformations also here
        # Even though we don't apply transformations here, we set two empty lists to tfms. Train and Validation augmentations
        .databunch(bs=BATCH_SIZE)
        # convert to databunch
        .normalize([tensor([0.702447, 0.546243, 0.696453]), tensor([0.238893, 0.282094, 0.216251])])
        # Normalize with training set stats. These are means and std's of each three channel and we calculated these previously in the stats step.
       )
	   
data = (ImageItemList.from_df('/content', df=train_df, folder='train', suffix='.tif')
         .split_by_idx(val_idx)

        .label_from_df(cols='label')
        
        .transform(tfms=[[],[]], size=sz)
        
        .databunch(bs=BATCH_SIZE)
        # convert to databunch
        .normalize([tensor([0.702447, 0.546243, 0.696453]), tensor([0.238893, 0.282094, 0.216251])])
        # Normalize with training set stats. These are means and std's of each three channel and we calculated these previously in the stats step.
       )
	   
imgDataBunch.show_batch(rows=2, figsize=(4,4))
